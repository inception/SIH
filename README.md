## PPT 

regarding ppt:
- nested points instead of single line. for eg:
  - a point
    - nested point about above point
      - another nested point about the nested point above
- PLEASE use your head to fit the points in their proper place
- don't have to use exact wordings from here, make better - formal phrases
- to the point
- use trending key words and highlight them, for tracking system
- point must be cleary show our approach
- what problems we are trying to solve/ why to use (milestoneS/show stoppers)
- major use cases
- small points intsead of big paragraphs
- winning ppt: https://drive.google.com/file/d/1TA47-oROWakfU1KEpLRY08L4fgmDUNuV/view
- Our ppt:
  - https://sih.gov.in/uploads/template/INCEPTION220231008211422.pdf
  - https://sih.gov.in/uploads/template/INCEPTION220231002221317.pdf

world heritage site:
https://en.wikipedia.org/wiki/List_of_World_Heritage_Sites_in_India

## Data on idea

the project should be convertable to static site, [here](https://mattgreer.dev/blog/how-i-built-this-static-site-with-nextjs/)

These kind of project exist, why u choose to build it ?:
- we can't compete with existing data, everything exist but in a scatterd way
- Aim: to gather all available data under a single but organised webapp
- putting together data unavailable on internet, will put out project on the risk of legitimacy. As, it wont be easy to verify something unavailable on trusted sites.

user friendly:
- easy to navigate, self explanatory UI
- mostly components will have their own path (URI) for easy/direct sharing of specific sections

need:
- one stop for all info u need about a region.
- to keep the heritage of our country alive
- interactive map and images for better visualization
- interactive & fun way to learn about our country. 
- attract tourist

as separate app from browser regardless of platform:
- will  be a progressive webapp
- click on install/add to home screen
- the webapp will work like separate app from browser
- if offline content will be shown from cache
- benefits: 
  -  doesn't depend on any appstore
  - always up to date
  - platform independent, so no specific optimizations needed
 
Privacy:
- we'll wont be collecting any user specific 
- just visits to relate and suggests based on past interactions
- once complete the project will be open sourced under Apache licence (for commercial purpose)
- anyone can verify how the project works and contribute

SPA:
- no reloading
- will reduce continuous load on server
- enhancing user experience

 Business potential:
- ultimate framework to showcase about regions with help of interactive maps (not limited to India). Just needs the svg and data.
- free for non-commercial individual use
- businesses can buy our paid API, to show their services on relevant sections. Which will then get ranked based on reviews.
- "weekend trip planning" still doesn't sound good to me, unless we can provide route, expenses, stay etc.
It might be a final milestone and part of business model which goes hand in hand with other companies, providing above mentioned services.


## flow of webapp

images: [here](./images)

```mermaid
graph LR
K[website] -->
W[mobile, pc]
W -->
A[map of india] <-- Select state --> 
B((state map)) <-- Select district -->  
C(page1 page2)
W -->
D(Install) -->  
E(Open app) -->
F(Own Window) -->
A
```


## Tech Stack

Git:
- version control

Android:
- Our webApp will scale according to screen ratio/ resolution. To use our webapp simply as an android app just click on "add to home screen", this will allow u to use it as an individual app separate from browser (advantages: platform independent, no need to optimize and update according to platform.
- show blog
- show in phone how to use it as separate app

css & html:
    

Js: 
- since all team members are not comfortable with Ts, NextJS will allow us to use combination of both simultaniously
    

Ts: 
- it's a statically typed lang, will help us catch any type errors before hand. Our frame work will ultimately convert Ts to JS as browser doesn't directly understand Ts.
    
Tailwind: 
- for styling
- allows to quickly build designs by appending a set of pre-defined CSS utility classes.

React : 
- lead devs are comfortable with react

Next:
- recommended framework by react, will us keep the code organised and maintainable. As, it already has a defined structure to work on.
- *show blog repo

AI: 
- data verification/suggestion/quiz
- *currently ChatGpt API is showing rate limited exceeded, we will highly likely use other API or tool
- maybe a visual platform like Botpress

Python:
- data scraping
- there are a lot of lib available in python which can help us store the output in any desired format.

Shell (UNIX/LInux) : 
- CI/CD (automated testing), hosting and more
       
SEO (Search Engine Optimization):
    
- using ratio instead of pixels, for an even experiene through out devices
    
- CLS (Cumulative layout shift) : sudden shift of UI elements, hindering the user exp
    
	- sol: reserving space for moving/shifting/poping elements
    

-  LCP (largest contentful paint) : media taking long to load, there's delay
    
	- sol: lossless image format (webp, Avif)
    

NodeJS: 
- to get our app running
    

Sql :
-  to interact with database, preferrably sqlite
-  (advantages: serverless functioning, a single file will serve as database) should enough for our data


============================
